var express=require('express');
var request=require('request');
var async=require('async');
var htmlparser = require("htmlparser2"); //html parsing library 
var domutils=require('domutils'); // utilities to be used with htmlparser library
var fs=require('fs');
var app=express();
var apidata=require("./apidata.js");
var url_indeed_api="http://api.indeed.com/ads/apisearch?"+
"publisher=1237714771010530&latlong=1&v=2&format=json";
var url_usajobs_api="https://data.usajobs.gov/api/search?";
var url_github_api="https://jobs.github.com/positions.json?";
var url_neuvoo_api="http://neuvoo.ca/jobs/?k=";
app.use('/client', express.static(__dirname + "/client"));
app.set('view engine','ejs');
app.get('/',function(req,res){
    apidata.getData("java","","",res);
    
});
app.get('/data',function(req,res){
    fs.readFile('/lab/job/client/data.csv', 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
    res.type('csv');
    res.send(data);
});
});
app.get('/about',function(req,res){
	res.render('pages/about');
});

/*
app.get('/search',function(req,res){
    var neuvoo_json=[];
    request(url_neuvoo,function(error,response,body){
        var handler = new htmlparser.DomHandler(function (error, dom) {
        if (error){
            
        }           
        else{
              
               var json_data={};
               var elements= domutils.getElements({itemprop:"title"},dom);
               var company= domutils.getElements({itemprop:"name"},dom);
               var addrLocation= domutils.getElements({itemprop:"addressLocality"},dom);
               var addrRegion=domutils.getElements({itemprop:"addressRegion"},dom);
               var imgurl=domutils.getElements({class:"j-logo"},dom);
               var description=domutils.getElements({itemprop:"description"},dom);
               var joburl=domutils.getElements({class:"gojob"},dom);
               for(var i=0;i<elements.length;i++){
                    json_data["title"]=elements[i].children[0].data;
                    json_data["url"]="http://neuvoo.ca/"+joburl[i].attribs.href;
                    json_data['company']=company[i].children[0].data;
                    json_data['locality']=addrLocation[i].children[0].data;
                    json_data['region']=addrRegion[i].children[0].data;
                    json_data['logo_url']="http://neuvoo.ca/"+imgurl[i].attribs.src;
                    json_data['description']=description[i].children[0].data;
                    neuvoo_json.push(JSON.parse(JSON.stringify(json_data)));
                }
                
                
                
                
               
                
        }
            
            
    });
    var parser = new htmlparser.Parser(handler);
    parser.write(body);
    parser.done();
        
    });
	request(url_indeed,function(error,response,body){
		if (!error && response.statusCode == 200) {
				var indeed_json=JSON.parse(body);               
				console.log(indeed_json);                
                var img_source="/client/indeed.jpeg"
                //return json_data;
                request(url_github,function(error,response,body){
                    var github_json=JSON.parse(body);
                    //res.json(github_json);
                      request({
                            url:url_usajobs,
                            method:'GET',
                            headers:{
                              'Host':'data.usajobs.gov',
                               'User-Agent':'nivanphp@gmail.com',
                               'Authorization-Key':'VkmKlIUSKY04V3ZPg9hV+n7mArWueGZt9GnJKWRDIhU='
                            }
                          },function(error,response,body){
                              var usa_json=JSON.parse(body);
                              //res.json(JSON.stringify(usa_json["SearchResult"]["SearchResultItems"][1]["MatchedObjectDescriptor"]));
                              //res.json(json_data["SearchResult"]["SearchResultItems"]);
                              //console.log(neuvoo_json);
                              res.render("pages/search",{jobs:indeed_json["results"],
                                                         usa_jobs:usa_json["SearchResult"]["SearchResultItems"],
                                                         github_jobs:github_json,
                                                         neuvoo_jobs:neuvoo_json});
                          });
                });
                
                //res.render("pages/search",{jobs:json_data["results"],indeed_img:img_source});
		 }     
         
	}); 
   
    
});*/
app.get("/search",function(req,res){
   reqvalues={keyword:req.query.keyword,location:req.query.location,country:req.query.country};
   url_indeed=url_indeed_api+"&q="+reqvalues["keyword"]+"&l="+reqvalues["location"]+"&co="+reqvalues["country"];
   url_neuvoo=url_neuvoo_api+reqvalues["keyword"];
   url_github=url_github_api+"search="+reqvalues["keyword"]+"&location="+reqvalues["location"];
   url_usajobs=url_usajobs_api+"Keyword="+reqvalues["keyword"]+"&LocationName="+reqvalues["location"];
   var neuvoo_json=[];
    request(url_neuvoo,function(error,response,body){
        var handler = new htmlparser.DomHandler(function (error, dom) {
        if (error){
            
        }           
        else{
              
               var json_data={};
               var elements= domutils.getElements({itemprop:"title"},dom);
               var company= domutils.getElements({itemprop:"name"},dom);
               var addrLocation= domutils.getElements({itemprop:"addressLocality"},dom);
               var addrRegion=domutils.getElements({itemprop:"addressRegion"},dom);
               var imgurl=domutils.getElements({class:"j-logo"},dom);
               var description=domutils.getElements({itemprop:"description"},dom);
               var joburl=domutils.getElements({class:"gojob"},dom);
               for(var i=0;i<elements.length;i++){
                    json_data["title"]=elements[i].children[0].data;
                    json_data["url"]="http://neuvoo.ca/"+joburl[i].attribs.href;
                    json_data['company']=company[i].children[0].data;
                    json_data['locality']=addrLocation[i].children[0].data;
                   // json_data['region']=addrRegion[i].children[0].data;
                    json_data['logo_url']="http://neuvoo.ca/"+imgurl[i].attribs.src;
                    json_data['description']=description[i].children[0].data;
                    neuvoo_json.push(JSON.parse(JSON.stringify(json_data)));
                }
                
                
                
                
               
                
        }
            
            
    });
    var parser = new htmlparser.Parser(handler);
    parser.write(body);
    parser.done();
        
    });
	request(url_indeed,function(error,response,body){
		if (!error && response.statusCode == 200) {
				var indeed_json=JSON.parse(body);               
				console.log(indeed_json);                
                var img_source="/client/indeed.jpeg"
                //return json_data;
                request(url_github,function(error,response,body){
                    var github_json=JSON.parse(body);
                    //res.json(github_json);
                      request({
                            url:url_usajobs,
                            method:'GET',
                            headers:{
                              'Host':'data.usajobs.gov',
                               'User-Agent':'nivanphp@gmail.com',
                               'Authorization-Key':'VkmKlIUSKY04V3ZPg9hV+n7mArWueGZt9GnJKWRDIhU='
                            }
                          },function(error,response,body){
                              var usa_json=JSON.parse(body);
                              //res.json(JSON.stringify(usa_json["SearchResult"]["SearchResultItems"][1]["MatchedObjectDescriptor"]));
                              //res.json(json_data["SearchResult"]["SearchResultItems"]);
                              //console.log(neuvoo_json);
                              res.render("pages/search",{jobs:indeed_json["results"],
                                                         usa_jobs:usa_json["SearchResult"]["SearchResultItems"],
                                                         github_jobs:github_json,
                                                         neuvoo_jobs:neuvoo_json});
                          });
                });
                
                //res.render("pages/search",{jobs:json_data["results"],indeed_img:img_source});
		 }     
         
	}); 
});

app.listen(3000);
console.log('3000 is magic port');	
